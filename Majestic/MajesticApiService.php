<?php
namespace Epiphany\MajesticSeoBundle\Majestic;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class MajesticApiService
 * @package Epiphany\MajesticSeoBundle\Majestic
 *
 * API Commands https://developer-support.majestic.com/api/commands/
 *
 * @method  mixed getBackLinkData($name, $arguments)
 * @method  mixed getAnchorText($name, $arguments)
 * @method  mixed getIndexItemInfo($name, $arguments)
 * @method  mixed AddToBucket($name, $arguments)
 * @method  mixed GetKeywordInfo($name, $arguments)
 * @method  mixed GetBackLinksHistory($name, $arguments)
 * @method  mixed GetHostedDomains($name, $arguments)
 * @method  mixed GetLinkProfile($name, $arguments)
 * @method  mixed GetNewLostBackLinks($name, $arguments)
 * @method  mixed GetNewLostBackLinkCalendar($name, $arguments)
 * @method  mixed GetRefDomains($name, $arguments)
 * @method  mixed GetRefDomainInfo($name, $arguments)
 * @method  mixed GetTopics($name, $arguments)
 * @method  mixed GetTopPages($name, $arguments)
 * @method  mixed GetSubscriptionInfo($name, $arguments)
 * @method  mixed GetDownloadsList($name, $arguments)
 * @method  mixed DeleteDownloads($name, $arguments)
 * @method  mixed DownloadBackLinks($name, $arguments)
 * @method  mixed DownloadRefDomainBackLinks($name, $arguments)
 * @method  mixed SearchByKeyword($name, $arguments)
 * @method  mixed SubmitURLsToCrawl($name, $arguments)
 */
class MajesticApiService
{
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var string
     */
    private $responseType;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var Client
     */
    private $guzzleClient;

    /**
     * MajesticApiService constructor.
     *
     * @param string $apiKey
     * @param string $endpoint
     * @param Client $guzzleClient
     * @param string $responseType
     */
    public function __construct(
        $apiKey,
        $endpoint,
        Client $guzzleClient,
        $responseType
    ) {
        $this->apiKey = $apiKey;
        $this->endpoint = $endpoint;
        $this->guzzleClient = $guzzleClient;
        $this->responseType = $responseType;
    }

    /**
     * @param Client $client
     * @throws \Exception
     */
    public function setClient(Client $client)
    {
        $this->guzzleClient = $client;
    }

    /**
     * @param string $name
     * @param array  $arguments
     * @return ResponseInterface
     */
    public function __call($name, $arguments)
    {
        $command = ucfirst($name);
        if (isset($arguments[1])) {
            $params  = $arguments[1];
        } else {
            $params = array();
        }
        if (is_string($arguments[0])) {
            $params['item'] = $arguments[0];
        } elseif (is_array($arguments[0])) {
            $counter = 0;
            foreach ($arguments[0] as $url) {
                $params['item' . $counter] = $url;
                $counter++;
            }
            $params['items'] = $counter;
        }
        return $this->executeCommand($command, $params);
    }

    /**
     * Execute Command
     * Makes the call to the api and returns the result
     *
     * @param  string $command
     * @param  array  $params
     * @return ResponseInterface
     */
    public function executeCommand($command, $params = array())
    {
        $params["cmd"]         = $command;
        $params["app_api_key"] = $this->apiKey;

        return $this->guzzleClient->get(
            $this->endpoint ."/". $this->responseType,
            [
            'query' => $params
            ]
        );
    }
}
