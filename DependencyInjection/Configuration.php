<?php

namespace Epiphany\MajesticSeoBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('epiphany_majestic_seo');

        $rootNode
            ->children()
            ->scalarNode('apiKey')->defaultNull()
            ->end()
            ->scalarNode('endpoint')->defaultValue('http://developer.majestic.com/api')
            ->end()
            ->scalarNode('guzzle')->defaultNull()
            ->end()
            ->scalarNode('responseType')->defaultValue('json')
            ->end()
            ->end();



        return $treeBuilder;
    }
}
