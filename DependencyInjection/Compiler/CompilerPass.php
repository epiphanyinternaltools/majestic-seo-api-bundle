<?php
namespace Epiphany\MajesticSeoBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $clientService = str_replace('@', '', $container->getParameter('epiphany_majestic_seo.guzzle'));
        if ($clientService) {
            $def = $container->getDefinition('epiphany_majestic_seo');
            $def->addMethodCall('setClient', array(new Reference($clientService)));
        }
    }
}
