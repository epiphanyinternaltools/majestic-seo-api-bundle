<?php
namespace Epiphany\MajesticSeoBundle\Tests\Majestic;

use Epiphany\MajesticSeoBundle\Majestic\MajesticApiService;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class MajesticApiServiceTest extends TestCase
{

    public function testGetBackLinkDataCommand()
    {
        $client = $this->prophesize(Client::class);

        $majesticApiService = $this->getMajesticApiService($client);

        $params = array(
            'Count' => 100,
            'Mode' => 1,
            'datasource' => 'historic',
            'MaxSourceURLsPerRefDomain' => 1
        );

        $clientParams = array(
            'Count' => 100,
            'Mode' => 1,
            'datasource' => 'historic',
            'item' => 'everydoghasitsday.com',
            'cmd' => 'GetBackLinkData',
            'app_api_key' => 'api_key',
            'MaxSourceURLsPerRefDomain' => '1'
        );

        // set our expectations
        $client->get(
            'http://mock.majestic.com/json',
            [
            'query' => $clientParams
            ]
        )->shouldBeCalled();

        $url = 'everydoghasitsday.com';
        $majesticApiService->getBackLinkData($url, $params);
    }


    public function testAnchorTextDataCount()
    {
        $client = $this->prophesize(Client::class);

        $majesticApiService = $this->getMajesticApiService($client);

        $params = array(
            'Count' => 1000
        );

        $clientParams = array(
            'Count' => 1000,
            'item' => 'everydoghasitsday.com',
            'cmd' => 'GetAnchorText',
            'app_api_key' => 'api_key'
        );

        // set our expectations
        $client->get(
            'http://mock.majestic.com/json',
            [
            'query' => $clientParams
            ]
        )->shouldBeCalled();

        $url = 'everydoghasitsday.com';
        $majesticApiService->getAnchorText($url, $params);
    }


    public function testGetIndexItemInfo()
    {
        $client = $this->prophesize(Client::class);

        $majesticApiService = $this->getMajesticApiService($client);

        $params = array(
            'items' => 1,
            'Mode' => 1,
            'datasource' => 'fresh'
        );

        $clientParams = array(
            'items' => 1,
            'Mode' => 1,
            'datasource' => 'fresh',
            'item0' => 'everydoghasitsday.com',
            'cmd' => 'GetIndexItemInfo',
            'app_api_key' => 'api_key'
        );

        // set our expectations
        $client->get(
            'http://mock.majestic.com/json',
            [
            'query' => $clientParams
            ]
        )->shouldBeCalled();

        $url = 'everydoghasitsday.com';
        $majesticApiService->getIndexItemInfo([$url], $params);
    }

    /**
     * @param $client
     * @return MajesticApiService
     */
    private function getMajesticApiService($client)
    {
        $majesticApiService = new MajesticApiService(
            'api_key',
            'http://mock.majestic.com',
            $client->reveal(),
            'json'
        );
        return $majesticApiService;
    }
}
