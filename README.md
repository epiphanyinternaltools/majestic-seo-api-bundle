Majestic SEO Symfony Bundle


Version History
============

##v0.1.5 - 10 January 2018
	 - Update

	 - Update composer

	 - Add scrutinizer config

	 - Refactoring & Tests

	 - Change

	 - Merge branch 'master' of https://bitbucket.org/epiphanyinternaltools/majestic-seo-api-bundle

	 - Fixes

	 - Fixes

	 - Amends

	 - Update authors

	 - Initial commit

=====================

Symfony Bundle for making request to the Majestic SEO API

This bundle is also available via [composer](https://github.com/composer/composer), find it on [packagist](https://packagist.org/packages/epiphany/majestic-seo-bundle).

## Installation ##

Add the `epiphany/majestic-seo-bundle` package to your `require` section in the `composer.json` file.

``` bash
$ composer require epiphany/majestic-seo-bundle
```

Add the EpiphanyMajesticSeoBundle to your application's kernel:

``` php
<?php
public function registerBundles()
{
    $bundles = array(
        // ...
        new Epiphany\MajesticSeoBundle\EpiphanyMajesticSeoBundle(),
        // ...
    );
    ...
}
```


## Configuration ##

config.yml 
``` yaml
epiphany_majestic_seo:
    endpoint: 'https://developer.majestic.com/api'
    apiKey: 'AH00000I11355882'
    # optional, you can specify the guzzle client that will be used for the requests
    # guzzle: '@eight_points_guzzle.client.default' 
```

services.yml
``` yaml
app.data_fetcher.data_fetcher:
    class: AppBundle\DataFetcher\DataFetcher
    arguments: ['@epiphany_majestic_seo']
    
```

## Usage ##

``` php
$params = [
    'MaxSameSourceURLs' => 1
];
        
$response = $this->majesticApiService->getBackLinkData('test.com', $params);

```

``` php
$params = [
    'Count' => 1000
];

$response = $this->majesticApiService->getAnchorText('test.com', $params);
```


``` php
$params = [
    'Count' => 10000,
    'Mode' => 1,
    'datasource' => 'fresh',
    'MaxSourceURLsPerRefDomain' => 1
];
$response = $this->majesticApiService->getBackLinkData('test.com', $params);
```


### Majestic Seo Api Commands
To see a full list of all available API commands please check the official documentation https://developer-support.majestic.com/api/commands/
